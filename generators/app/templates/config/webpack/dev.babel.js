import merge from 'webpack-merge';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import LoaderOptionsPlugin from 'webpack/lib/LoaderOptionsPlugin';
import baseConf from './base.babel';

export default merge(baseConf, {
  devtool: 'source-map',
  cache: true,
  module: {
    loaders: [{
      test: /\.s?css$/,
      exclude: /node_modules/,
      loader: ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: `css-loader!sass-loader?${['outputStyle=expanded'].join('&')}`
      })
    }]
  },
  plugins: [
    new LoaderOptionsPlugin({
      debug: true
    })
  ]
});
